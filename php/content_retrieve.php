<?php

include 'config.php';

if (isset($_POST['name']))

{

    $name=$_POST['name'];
    
    $sql=$con->prepare("SELECT * FROM ak_table WHERE name = ?");

    $sql->execute(array($name));

    $sql->setFetchMode(PDO::FETCH_ASSOC);

    if($sql->rowCount() >0)

    {
    
        while($row = $sql->fetch()){
    
            $item=$row;
    
            $json = json_encode($item);
            
        }

    }
    
    else
    
    {
    
        echo "No Results Found";
    
    }
    
    echo $json;
    
    $con=NULL;

}